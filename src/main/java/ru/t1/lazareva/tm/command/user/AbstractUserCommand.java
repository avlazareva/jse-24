package ru.t1.lazareva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.service.IAuthService;
import ru.t1.lazareva.tm.api.service.IUserService;
import ru.t1.lazareva.tm.command.AbstractCommand;
import ru.t1.lazareva.tm.exception.entity.UserNotFoundException;
import ru.t1.lazareva.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    public @NotNull IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Override
    public String getArgument() {
        return null;
    }

}